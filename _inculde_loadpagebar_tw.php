<?php
		$p = $_GET[p];
		//取得所有數量
		$total = $DB -> get_count("select notice_id from notice ");

		//幾筆分一頁
		$per = 10;
		//中間頁數的數量
		$shownum =2;
		//取得所有資料
		$sql = "select * from notice order by notice_id asc limit ".($per*$p).",".$per;
		$result = $DB -> get_result($sql);
		
		//取得分頁數量
		$page_num = ceil($total / $per);
		//連結網址
		$target_url ="'.$target_url.'";
?>

<div class="ipage2">
  	<ul>
  		<?php 
  		if($total>0)
  		{
  			if($p>0)
  			echo '<li><a href="'.$target_url.''.'0'.'">第一頁</a></li>';
			
  			if($p>0)
  			echo '<li><a href="'.$target_url.''.($p-1).'">上一頁</a></li>';
  			else
  			echo '<li>上一頁</li>';
			
			//中間顯示頁數
			if ($page_num > $shownum) {
			if ($p >= ($page_num - 2))//最後三頁
			{
				$start_num = $page_num - $shownum;
				//迴圈起始值
				$loop_amount = $page_num;
				//迴圈終止值
			} else {
				if ($p <= 2) {
					$start_num = 0;
					//迴圈起始值
					$loop_amount = $shownum;
					//迴圈終止值
				} else {
					$start_num = $p - 2;
					//迴圈起始值
					$loop_amount = $p + 3;
					//迴圈終止值
				}
			}
		} else//沒有大於五筆
		{
			$start_num = 0;
			//迴圈起始值
			$loop_amount = $page_num;
			//迴圈終止值
		}
		for ($i = $start_num; $i < $loop_amount; $i++) {
			if ($i == $p)//當前頁
			{
				$current = ' pages_current';
				echo '<li>'. ($i + 1).'<li>';
			} else {
				$current = '';
				echo '<li><a href="'.$target_url.''. $i . '">' . ($i + 1) . '</a><li>';
			}
		}
		if (($p + 1) >= $page_num) {
			echo '<li>下一頁<li>';

		} else {
			echo '<li><a href="'.$target_url.''. ($p+1) . '">下一頁</a><li>';
			echo '<li><a href="'.$target_url.''. ($page_num-1). '">最後一頁</a><li>';
			
		}
  		}
  		?>
    	<li>(共<?php echo $total; ?>筆)</li>
        <br clear="all" />
    </ul>
</div>