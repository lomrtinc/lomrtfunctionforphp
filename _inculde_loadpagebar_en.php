<?php
		$p = $_GET[p];
		//get the amount of data
		$total = $DB -> get_count("select id from news ");

		//one page can show how many data
		$per = 10;
		
		//the number of page
		$shownum =2;
		
		//get all data
		$sql = "select * from news order by id asc limit ".($per*$p).",".$per;
		$result = $DB -> get_result($sql);
		
		//count how many  page
		$page_num = ceil($total / $per);

		//target url
		$target_url ="test.php?p=";
?>

<div class="ipage2">
  	<ul>
  		<?php 
  		if($total>0)
  		{
  			if($p>0)
  			echo '<li><a href="'.$target_url.''.'0'.'">第一頁(First Page)</a></li>';
			
  			if($p>0)
  			echo '<li><a href="'.$target_url.''.($p-1).'">上一頁(Last Page)</a></li>';
  			else
  			echo '<li>上一頁(Last Page)</li>';
			
			//the number of page
			if ($page_num > $shownum) {
			if ($p >= ($page_num - 2))//last three page
			{
				$start_num = $page_num - $shownum;
				//loop start
				$loop_amount = $page_num;
				//loop stop
			} else {
				if ($p <= 2) {
					
					//start value of loop
					$start_num = 0;
					//the end value of loop
					$loop_amount = $shownum;
				} else {
					
					//start value of loop
					$start_num = $p - 2;
					
					//the end value of loop
					$loop_amount = $p + 3;
				}
			}
		} else//least five
		{
			//start value of loop
			$start_num = 0;
			
			//the end value of loop
			$loop_amount = $page_num;
		}
		for ($i = $start_num; $i < $loop_amount; $i++) {
			if ($i == $p)//now load page
			{
				$current = ' pages_current';
				echo '<li>'. ($i + 1).'<li>';
			} else {
				$current = '';
				echo '<li><a href="'.$target_url.''. $i . '">' . ($i + 1) . '</a><li>';
			}
		}
		if (($p + 1) >= $page_num) {
			echo '<li>下一頁(Next Page)<li>';

		} else {
			echo '<li><a href="'.$target_url.''. ($p+1) . '">下一頁(Next Page)</a><li>';
			echo '<li><a href="'.$target_url.''. $page_num . '">最後一頁(Last Page)</a><li>';
			
		}
  		}
  		?>
    	<li>(共<?php echo $total; ?>筆)</li>
        <br clear="all" />
    </ul>
</div>